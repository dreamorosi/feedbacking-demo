import React from 'react'

class Tasks extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      task: props.task
    }
  }

  renderTask () {
    const { task } = this.state
    console.log(task)
    if (Object.keys(task).length > 0) {
      return (
        <tr>
          <td><i className='fas fa-square' /> {task.title}</td>
          <td><i className='far fa-user-circle fa-2x' /></td>
          <td><span>High</span></td>
          <td>{task.date}</td>
          <td><i className='fa fa-circle' /> New</td>
        </tr>
      )
    }
  }

  render () {
    return (
      <div className='task'>
        <table>
          <thead>
            <tr>
              <th>This Week</th>
              <th>Person</th>
              <th>Priority</th>
              <th>Date</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><i className='fas fa-square' /> I don't like this headline!</td>
              <td><i className='far fa-user-circle fa-2x' /></td>
              <td><span>High</span></td>
              <td>20 / 02 / 2018</td>
              <td><i className='fa fa-circle' /> Done</td>
            </tr>
            {this.renderTask()}
            <tr>
              <td>Add a new task...</td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}

export default Tasks
