import React from 'react'

import AddTask from './AddTask'

class Feedback extends React.Component {
  constructor () {
    super()

    this.state = {
      isOpen: false,
      tasks: [],
      current: -1
    }

    this.handleClick = this.handleClick.bind(this)
  }

  componentDidMount () {
    const dummy = window.fetch('http://feedbacking.s3-website-eu-west-1.amazonaws.com/sample-db.json')
    const live = window.fetch('http://dev.dreamorosi.com/feedbacking-db/read')

    Promise.all([dummy, live])
      .then(results => results.map(res => res.json()))
      .then(res => {
        Promise.all(res)
          .then(el => {
            const { feedbacks } = el[0]
            feedbacks.push({
              title: el[1].feedback,
              date: '06 / 07 / 2018',
              location: el[1].target
            })

            this.setState({ tasks: feedbacks })
          })
      })
      .catch(err => console.log(err))
  }

  handleClick (index = -1) {
    const { isOpen, tasks, current } = this.state

    this.props.makeTask(tasks[current])

    this.setState({
      isOpen: !isOpen,
      current: index
    })
  }

  renderAdd () {
    const { isOpen, tasks, current } = this.state
    if (isOpen) {
      return <AddTask content={tasks[current]} closeAdd={this.handleClick} />
    } else {
      return false
    }
  }

  trimLocation (location) {
    if (location.length > 25) {
      return location.substr(0, 25) + '...'
    } else {
      return location.split(':')[0]
    }
  }

  render () {
    const { isOpen, tasks } = this.state
    const openClass = isOpen ? 'open' : ''
    return (
      <div>
        <div className={`feedback ${openClass}`}>
          <table>
            <thead>
              <tr>
                <th>This Week</th>
                <th>Date</th>
                <th>Sender</th>
                <th>Assign</th>
                <th>Status</th>
                <th>Priority</th>
                <th>Location</th>
              </tr>
            </thead>
            <tbody>
              {tasks.map((task, i) => {
                return (
                  <tr key={i}>
                    <td onClick={() => this.handleClick(i)}>
                      <i className='fas fa-tasks' /> {task.title}
                    </td>
                    <td>{task.date}</td>
                    <td><i className='far fa-user-circle fa-2x' /></td>
                    <td><i className='far fa-user-circle fa-2x' /></td>
                    <td><i className='fa fa-circle' /> Set</td>
                    <td><button>Set</button></td>
                    <td>{this.trimLocation(task.location)}</td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
        {this.renderAdd()}
      </div>
    )
  }
}

export default Feedback
