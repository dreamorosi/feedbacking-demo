import React from 'react'

class AddTask extends React.Component {
  constructor () {
    super()

    this.state = {
      isDateSet: false,
      isPrioritySet: false
    }
  }

  renderPriority (isPrioritySet) {
    if (isPrioritySet) {
      return <span style={{ color: '#CE4257', marginRight: 0, border: '1px solid #CE4257', padding: '0 5px' }}>HIGH</span>
    } else {
      return <span style={{ marginRight: 0 }}>Set <i className='fas fa-caret-down' /></span>
    }
  }

  renderDate (isDateSet) {
    if (isDateSet) {
      return '09 / 07 / 2018'
    } else {
      return <span style={{ marginRight: 0 }}>Set</span>
    }
  }

  render () {
    const { isDateSet, isPrioritySet } = this.state
    const { content, closeAdd } = this.props
    const { title, location } = content

    return (
      <div className='add-task'>
        <span>Task:</span>
        <p>{title}</p>
        <p><span>Location:</span> {location}</p>
        <p><span>Status:</span> <i className='fa fa-circle' /> <i className='fas fa-caret-down' /></p>
        <p onClick={() => this.setState({ isPrioritySet: !isPrioritySet })}>
          <span>Priority:</span> {this.renderPriority(isPrioritySet)}
        </p>
        <p><span>Assigned To:</span> Assign <i className='fas fa-caret-down' /></p>
        <p><span>Reported By:</span> User</p>
        <p onClick={() => this.setState({ isDateSet: !isDateSet })}>
          <span>Due Date:</span> {this.renderDate(isDateSet)}
        </p>
        <p><span>Tags:</span> <button>add</button></p>
        <p>Comments:</p>
        <textarea name='name' rows='2' />
        <button onClick={closeAdd}>Make Task</button>
      </div>
    )
  }
}

export default AddTask
