import React from 'react'
import { render } from 'react-dom'

import Feedback from './Feedback'
import Tasks from './Tasks'

class App extends React.Component {
  constructor () {
    super()

    this.state = {
      current: 'feedback',
      task: {}
    }

    this.setTask = this.setTask.bind(this)
  }

  handleClick (current) {
    this.setState({ current: current })
  }

  setTask (data) {
    this.setState({ task: data })
  }

  renderCard (card) {
    const { task } = this.state

    switch (card) {
      case 'feedback':
        return <Feedback key={1} makeTask={this.setTask} />
      case 'tasks':
        return <Tasks key={1} task={task} />
    }
  }

  render () {
    const { current } = this.state
    return ([
      <div key={0}>
        <span
          className={current === 'feedback' ? 'active' : ''}
          onClick={() => this.handleClick('feedback')}>Feedback</span>
        <span
          className={current === 'tasks' ? 'active' : ''}
          onClick={() => this.handleClick('tasks')}>Tasks List</span>
        <span>Tasks Board</span>
        <span>Archive</span>
      </div>,
      this.renderCard(current)
    ])
  }
}

render(<App />, document.querySelector('#content-root'))
