/* global fetch */

;(function () {
  const handleClick = (e) => {
    if (!listenToClick) {
      return
    }
    if (e.shiftKey) {
      openComment(e)
    }
  }

  const getLineage = (node, lineage = []) => {
    let tag = node.tagName.toLowerCase()
    tag += node.id !== '' ? `#${node.id}` : ''
    tag += node.classList.length > 0 ? `.${Array.from(node.classList).join('.')}` : ''
    lineage.push(tag)
    if (node.tagName === 'BODY') {
      return lineage.reverse().join(' > ')
    }
    return getLineage(node.parentNode, lineage)
  }

  const openComment = e => {
    fBox.style.display = 'block'
    fBox.style.left = `${e.clientX - 50}px`
    fBox.style.top = `${e.clientY - 25}px`
    fBox.dataset.target = getLineage(e.target)
    listenToClick = false
    body.removeEventListener('click', handleClick, false)
    body.addEventListener('click', clickOutsideBox, false)
    fBtn.addEventListener('click', registerComment, false)
    fTextarea.addEventListener('keypress', handleType, false)
  }

  const closeComment = () => {
    fBox.style.display = 'none'
    fTextarea.value = ''
    listenToClick = true
    body.removeEventListener('click', clickOutsideBox, false)
    window.setTimeout(() => {
      body.addEventListener('click', handleClick, false)
    }, 1000)
  }

  const handleType = (e) => {
    if (e.keyCode !== undefined || e.which !== 13 || e.keyCode !== 13 || e.shiftKey) {
      return
    }
    registerComment()
  }

  const registerComment = (e) => {
    fTextarea.style.border = '1px solid black'
    if (fTextarea.value.trim() === '') {
      fTextarea.style.border = '1px solid red'
      return
    }
    fBtn.removeEventListener('click', registerComment, false)
    fTextarea.removeEventListener('keypress', handleType, false)
    // window.setTimeout(() => {
    //
    // }, 250)
    const payload = {
      'target': fBox.dataset.target.toString(),
      'feedback': fTextarea.value,
      'coords': {
        x: fBox.style.left,
        y: fBox.style.top
      },
      'timestamp': e.timeStamp
    }
    fetch('http://dev.dreamorosi.com/feedbacking-db/send', {
      method: 'POST',
      body: JSON.stringify(payload)
    })
      .then(res => res.json())
      .then(res => {
        let dot = document.createElement('div')
        dot.style.left = res.coords.x
        dot.style.top = res.coords.y
        dot.style.position = 'absolute'
        dot.style.display = 'block'
        dot.style.borderRadius = '50%'
        dot.style.width = '50px'
        dot.style.height = '50px'
        dot.style.background = '#CE4257'
        body.appendChild(dot)

        console.log(res)
      })
      .catch(err => console.log(err))
    closeComment()
  }

  const clickOutsideBox = (e) => {
    let lineage = getLineage(e.target)
    if (!lineage.match(/(#feedbacking-box)/gm)) {
      closeComment()
    } else {
      return true
    }
  }

  let fBox
  let fTextarea
  let fBtn
  let fBtnCancel
  let body
  let listenToClick = true

  const init = () => {
    let head = document.head
    let link = document.createElement('link')

    link.type = 'text/css'
    link.rel = 'stylesheet'
    link.href = './landing.css'
    head.appendChild(link)
    body = document.querySelector('body')

    // Create Feedback box
    fBox = document.createElement('div')
    fBox.id = 'feedbacking-box'
    let fBoxRow1 = document.createElement('div')
    let fBoxRow2 = document.createElement('div')
    fTextarea = document.createElement('textarea')
    fTextarea.placeholder = 'Your feedback here...'
    fBtn = document.createElement('button')
    fBtnCancel = document.createElement('button')
    fBtn.appendChild(document.createTextNode('Send'))
    fBtnCancel.appendChild(document.createTextNode('Cancel'))
    fBoxRow1.appendChild(fTextarea)
    fBoxRow2.appendChild(fBtnCancel)
    fBoxRow2.appendChild(fBtn)
    fBox.appendChild(fBoxRow1)
    fBox.appendChild(fBoxRow2)
    fBox.style.display = 'none'
    fBox.style.position = 'absolute'
    body.appendChild(fBox)

    body.addEventListener('click', handleClick, false)
    fBtnCancel.addEventListener('click', closeComment, false)
  }

  init()
})()
