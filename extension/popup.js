/* global chrome XMLHttpRequest */

let changeColor = document.getElementById('changeColor')

changeColor.onclick = function (element) {
  let color = element.target.value
  chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
    chrome.tabs.executeScript(
      tabs[0].id,
      {code: 'document.body.style.backgroundColor = "' + color + '";'})
  })

  var xhttp = new XMLHttpRequest()
  xhttp.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      // Typical action to be performed when the document is ready:
      // document.getElementById('demo').innerHTML = xhttp.responseText
      // alert(xhttp.responseText)
      console.log(xhttp.responseText)
    }
  }
  xhttp.open('GET', 'https://tk5mc5m0xb.execute-api.eu-west-1.amazonaws.com/test/feedbacking-hosts', true)
  xhttp.send()
}

chrome.storage.sync.get('color', function (data) {
  changeColor.style.backgroundColor = data.color
  changeColor.setAttribute('value', data.color)
})
