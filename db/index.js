const { send, json } = require('micro')
const url = require('url')
const level = require('level')
const promisify = require('then-levelup')
const cors = require('micro-cors')()

const db = promisify(level('feedbacks.db', {
  valueEncoding: 'json'
}))


module.exports = cors( async (request, response) => {
  const { pathname } = url.parse(request.url)

  // console.log(pathname)
  
  if (pathname === '/feedbacking-db/read') {
    let records = await readRecords()
    console.log(records)
    send(response, 200, records)
  } else if (pathname === '/feedbacking-db/send') {
    const payload = await json(request)

    let res = addRecord(payload)
    send(response, 200, payload)
  } else {
    send(response, 200, 'Hello World!')
  }
})

const addRecord = async (data) => {
  return await db.put('1', JSON.stringify(data))
}

const readRecords = async () => {
  let feedback
  try {
    feedback = await db.get('1')
  } catch (error) {
    console.log(error)
  }
  return feedback
}
